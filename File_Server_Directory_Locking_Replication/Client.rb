class Client

  require 'socket'              # Sockets are in standard library
  require 'benchmark'
  require 'fileutils'           #for creating client directory
  SIZE = 1024 * 1024 * 10       #for reading and writing files

  hostname = 'localhost'
  port = 8000
  s = TCPSocket.open(hostname, port)  #directory server

  s.puts "new client"           # inform directory server new client joining network
  while (line = s.gets.chomp)   # Read lines from the socket
    numClient=line              # directory server tells client its number
    break
  end

  clientName="client#{numClient}" #use number to assign client name
  puts "This is " + clientName    #print name
  FileUtils.mkdir_p(clientName)   #make the client a directory to download files into.

  received='0'    #need to initialise this to avoid an error.

  loop{   #start client loop. client is a state machine.
    state='waiting'

    puts "Press a to download a file, b to kill server or c for student details.\n"
    state=gets.chomp
    fileName='0'    #initialise this to avoid an error.
    fileAddress='0' #initialise this to avoid an error.

    if state=='a'   #download state
      puts 'Enter file to download and press enter.'
      puts 'Files available:'
      puts 'a.txt'  # system contains three txt files: a.txt, b.txt and c.txt
      puts 'b.txt'
      puts 'c.txt'
      fileName=gets.chomp #user inputs choice of file to download

      send=fileName
      s.puts send         #send choice to file server
      while (line = s.gets.chomp)   # Read port number of the file server storing master copy of file from directory server
        received=line
        break
      end
      while (line = s.gets.chomp)   # Read if file is locked or not from directory. Directory server gets this info from locking server.
        lock=line
        break
      end

      TCPSocket.open(hostname, Integer(received)) do |socket|   #connect to server containing file master copy.
        socket.puts "Download"                                  #inform file server this is a download request
        socket.puts fileName                                    #give name of file to file server
        fileAddress= clientName + '\\' + fileName               #e.g.  client0\a.txt
        File.open(fileAddress, 'w') do |file|                   #create a txt file in directory
          while chunk = socket.read(SIZE)
            file.write(chunk)                                   #write file to directory
            break
          end
        end
      end

      state='d'       #change to upload state

    elsif state=='b'  #choosing b shuts down the directory server, thus killing the whole system
      send="KILL_SERVER"
      s.puts send
      puts 'Server down. Exiting client.'
      sleep 1
      exit

    elsif state=='c'  #choosing c reads student details as requested in the brief for this project.
      send="HELO"
      s.puts send
      for i in 0..3   #details consist of four lines: HELO, IP, Port, and studentID.
        details=s.gets
        puts details
      end


    else
      puts "incorrect selection\n"    #insures user can only input a,b or c when prompted for input.
    end

    if state=='d'                     #this state is entered to re-upload the file we downloaded.
      if lock=="Unlocked"             #lock is only == "Unlocked" if we downloaded from the master copy.
        puts 'Download successful. '+ fileName + ' stored in ' + clientName + ' directory.'
        puts 'Once finished reading or writing file press enter to re-upload to file server'
        puts 'File will be deleted from ' + clientName + ' directory.'
        waiting=gets.chomp                                  #just used to wait for user to hit enter before continuing. Gives the user time to read and alter the document in the directory.
        TCPSocket.open(hostname, 8005) do |socket|          #open connection to replica server
          time = Benchmark.realtime do
            socket.puts fileName                            #send filename to replica server
            fileAddress=clientName + '\\' + fileName
            File.open(fileAddress, 'r') do |file|           #open file
              while chunk = file.read(SIZE)
                socket.write(chunk)                         #send file to replica server
              end
              socket.close                                  #close the socket to signal end of file
            end
          end
          File.delete(fileAddress)                          #delete file from directory
        end
      elsif lock=="Locked"                                  #if file was locked when request made then read only mode entered
        puts 'Download successful. '+ fileName + ' stored in ' + clientName + ' directory.'
        puts 'The file you have chosen is locked. You can read it but any changes you make will not be uploaded. Press enter to continue.'
        puts 'File will be deleted from ' + clientName + ' directory.'
        waiting=gets.chomp                                  #just used to wait for user to hit enter before continuing.
        File.delete(fileAddress)                            #delete file from directory
      else
        puts "Unlocked check failed"                        #to catch errors
      end
    end
  }#loop


end