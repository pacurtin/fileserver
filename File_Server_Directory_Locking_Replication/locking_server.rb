class LockingServer

  # MUST BE RUN FIRST. OTHERWISE DIRECTORY SERVER WON'T WORK.

  require 'thread'
  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        # Wait for space in the pool.
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            print "Pool is full; waiting to run #{args.join(',')}...\n" if $DEBUG
            # Sleep until some other thread calls @pool_cv.signal.
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            # Remove the thread from the pool.
            @pool.delete(Thread.current)
            # Signal the next waiting thread that there's a space in the pool.
            @pool_cv.signal
          end
        end
      end
    end
    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end
    def exception(thread, exception, *original_args)
      # Subclass this method to handle an exception within a thread.
      puts "Exception in thread #{thread}: #{exception}"
    end
  end

  pool=ThreadPool.new(10)                                                           #allows ten simultaneous socket connections.
  require 'socket'                                                                  # Get sockets from stdlib


  lockTable= Hash["a.txt", "Unlocked", "b.txt", "Unlocked", "c.txt", "Unlocked"]    #hashmap keeps track of currently locked files.


  server = TCPServer.open(8004)                                   # Socket to listen on port 8004
  loop {                                                          # Servers run forever
    Thread.start(server.accept) do |client|                       #new thread for each connection
      pool.dispatch(client) do |client|                           #add new thread to pool

        while line = client.gets.chomp                            # Read lines from either the directory or replication servers.
            if line.include?("Unlock")                            # Signifies the message is from the replication server meaning all copies of file up to date.
              n=line.length-6
              fileName=line[-n..-1]                               #read file name from message. Message example: "Unlocka.txt"
              puts "File name to unlock: "+fileName
              lockTable[fileName]= "Unlocked"                     #change locked file status back to unlocked
              puts fileName + " is " + lockTable[fileName]
            elsif lockTable.has_key?(line)                        #no "Unlock" in message means message had to have come from directory.
              puts "Lock information sent: " + line + " is " +lockTable[line]
              client.puts lockTable[line]                         #inform the directory of the files status
              if lockTable[line]== "Unlocked"                     #if directory knows a file is unlocked it will pass its information to the client...
                lockTable[line]= "Locked"                         #...if this happens the client opens the file. Therefore it must be marked locked.
                puts "File locked"
              end
            else
              client.puts "File does not exist."                  #to catch errors.
            end
        end
      end
    end
  }
end