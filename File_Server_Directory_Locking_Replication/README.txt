See DIAGRAM.png to see architecture.

Instructions for running:
1.Ensure replicaDir, fileServerDir, fileServerDir2 and fileServerDir3 are in project directory.
2.Ensure a.txt is in fileServerDir, b.txt is in fileServerDir2 and c.txt is in fileServerDir3.
3.Start locking_server first (DirectoryServer won't work).
4.Then start each of the other servers and as many instances of client as desired.
5.Downloaded files can be read or altered in the directory that's automaticaly created each time a client is started.

Classes:

Client: 
	Connects to directory server on running to receive unique ID.
	Creates its own directory with ID.
	Queries Directory to find port number of fileserver containing desired file download.
	Downloads file and stores it in its directory.
	If file was locked when downloaded it just deletes the file when user finished with it.
	If file was unlocked the file is uploaded to the replication server before deletion .
	Contains commands to display student information and kill server.

Directory Server:
	Keeps track of location of master copy of files.
	Query locking server for clients.

Locking Server:
	Tells the directory server if file is locked.
	If file is unlocked when query comes in locking server sends info to directory and then locks file.
	Unlocks file when replica server finished reuploading file.

Replica Server:
	Receives file from client.
	Uploads it to all file servers.
	Sends unlock message to Locking Server.

File Server:
	Serve files to clients.
	Receive files from replica server.
