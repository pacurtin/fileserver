class ReplicaServer

  SIZE = 1024 * 1024 * 10         # for reading and writing over sockets
  require 'socket'                # Get sockets from stdlib
  hostname = 'localhost'
  require 'benchmark'
  require 'thread'
  l = TCPSocket.open(hostname, 8004)  #locking server

  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        # Wait for space in the pool.
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            print "Pool is full; waiting to run #{args.join(',')}...\n" if $DEBUG
            # Sleep until some other thread calls @pool_cv.signal.
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            # Remove the thread from the pool.
            @pool.delete(Thread.current)
            # Signal the next waiting thread that there's a space in the pool.
            @pool_cv.signal
          end
        end
      end
    end
    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end
    def exception(thread, exception, *original_args)
      # Subclass this method to handle an exception within a thread.
      puts "Exception in thread #{thread}: #{exception}"
    end
  end

  pool=ThreadPool.new(10)

  server = TCPServer.open(8005)                           # Socket to listen on port 8005
  loop {                                                  # Servers run forever
    Thread.start(server.accept) do |client|               # start new thread to handle each client connection
      pool.dispatch(client) do |client|                   # add thread to pool
        while (line = client.gets.chomp)
          fileName=line                                   #filename sent first
          break
        end
        puts fileName + "received."
        fileAddress='replicaDir\\'+fileName
        File.open(fileAddress, 'w') do |file|             #write file to replica directory temporarily

          while chunk = client.read(SIZE)
            puts chunk
            file.write(chunk)
            break
          end
        end


        (8001..8003).each do |fileServerPorts|            # one by one connect to each file server
          TCPSocket.open(hostname, fileServerPorts) do |socket|
            time = Benchmark.realtime do
              socket.puts "Upload"                        # inform file server this is an upload request
              socket.puts fileName                        # give file server file name
              File.open(fileAddress, 'r') do |file|
                while chunk = file.read(SIZE)
                  puts chunk
                  socket.write(chunk)                     # send file to file server
                end
                socket.close                              #close the socket here so receiver knows when file finished sending so it can stop listening
              end
            end
          end
          puts fileName + " replicated to port #{fileServerPorts}"
        end

        File.delete(fileAddress)              #delete file to keep replica server free of clutter
        l.puts 'Unlock'+fileName              #unlock the file

      end
    end
  }

end