class DirectoryServer

  require 'socket'                # Get sockets from stdlib
  require 'thread'
  hostname = 'localhost'
  port = 8004
  l = TCPSocket.open(hostname, port)  #locking server

  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        # Wait for space in the pool.
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            print "Pool is full; waiting to run #{args.join(',')}...\n" if $DEBUG
            # Sleep until some other thread calls @pool_cv.signal.
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            # Remove the thread from the pool.
            @pool.delete(Thread.current)
            # Signal the next waiting thread that there's a space in the pool.
            @pool_cv.signal
          end
        end
      end
    end
    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end
    def exception(thread, exception, *original_args)
      # Subclass this method to handle an exception within a thread.
      puts "Exception in thread #{thread}: #{exception}"
    end
  end

  pool=ThreadPool.new(10)                                               #allows ten simultaneous socket connections.

  directory= Hash["a.txt", 8001, "b.txt", 8002, "c.txt", 8003]          #hashmap keeps track of where file master copies stored.
  numClients=0                                                          #keeps track of number of clients to assign each new client a unique name.

  server = TCPServer.open(8000)                                         # Socket to listen on port 2000
  loop {                                                                # Server runs forever
    Thread.start(server.accept) do |client|                             #new thread for each connection
      pool.dispatch(client) do |client|                                 #add new thread to pool

        while line = client.gets.chomp                                  #Read lines from client
          puts line
          if line=="KILL_SERVER"                        #kills the server if "KILL_SERVER" received from client.
            exit
          elsif line.include?("HELO")
            client.puts "#{line}\n" + "IP: #{Socket.gethostname}\n" + "Port: 8000\n" + "Student ID: 10323175\n"
          elsif line=="new client"
            client.puts numClients                      #send client its id number.
            numClients+=1                               #increment number of clients.
          else
            if directory.has_key?(line)
              l.puts line                               #query locking server for file status.
              while (lockLine = l.gets.chomp)           #Read lines from the locking server.
                lock=lockLine                           #store locking info in lock, "Locked" or "Unlocked".
                break
              end
              puts "Port number sent"
              client.puts directory[line]               #check hashmap using file name and send result to client.
              client.puts lock                          #pass on locking info to client.
              puts "Port number sent to client"         #write to console that info successfully sent to client.
            else
              client.puts "File does not exist."        #to catch errors
            end
          end

        end
      end
    end
  }

end