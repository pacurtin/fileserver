class FileServer2


  require 'thread'

  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        # Wait for space in the pool.
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            print "Pool is full; waiting to run #{args.join(',')}...\n" if $DEBUG
            # Sleep until some other thread calls @pool_cv.signal.
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            # Remove the thread from the pool.
            @pool.delete(Thread.current)
            # Signal the next waiting thread that there's a space in the pool.
            @pool_cv.signal
          end
        end
      end
    end
    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end
    def exception(thread, exception, *original_args)
      # Subclass this method to handle an exception within a thread.
      puts "Exception in thread #{thread}: #{exception}"
    end
  end

  pool=ThreadPool.new(3)
  require 'socket'                                      # Get sockets from stdlib
  require 'benchmark'
  SIZE = 1024 * 1024 * 10                               #for reading and writing files

  hostname = 'localhost'
  server = TCPServer.open(hostname,8002)                #listening on 8002
  loop {                                                # Servers run forever
    Thread.start(server.accept) do |client|
      pool.dispatch(client) do |client|
        while (line = client.gets.chomp)                # is incoming request upload or download
          order=line
          break
        end
        while (line = client.gets.chomp)                # filename for incoming request
          fileName=line
          break
        end
        puts order + " request received: " + fileName
        if order == "Download"                          # receiving this preceding a request means it's a download request from a client
          time = Benchmark.realtime do
            fileAddress='fileServer2Dir\\'+fileName
            File.open(fileAddress, 'r') do |file|       # open and read the requested file
              while chunk = file.read(SIZE)
                client.write(chunk)                     # send file to client
              end
              client.close                              # close the socket here so receiver knows when file finished sending so it can stop listening
            end
          end
        elsif order == "Upload"                         # receiving this preceding a request means it's a upload request from the replication server
          fileAddress='fileServer2Dir\\'+fileName
          File.open(fileAddress, 'w') do |file|
            while chunk = client.read(SIZE)
              file.write(chunk)                         # write file received to file server directory
              break
            end
          end
        else
          puts "Incorrect order."                       # to catch errors.
        end
      end
    end
  }

end